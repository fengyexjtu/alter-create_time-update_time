package com.fengye.generate.entity;

import lombok.Data;

@Data
public class TableNameInfo {

	private String field;

	private String type;

	private String nullValue;

	private String key;

	private String defaultValue;

	private String extra;

	// INSERT INTO `` (`Field`, `Type`, `Null`, `Key`, `Default`, `Extra`) VALUES ('id', 'int', 'NO', 'PRI',NULL, 'auto_increment');

}
