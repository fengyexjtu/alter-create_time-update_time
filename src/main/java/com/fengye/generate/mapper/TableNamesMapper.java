package com.fengye.generate.mapper;


import com.fengye.generate.entity.TableNameInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TableNamesMapper {

	List<String> getTableNames();
	TableNameInfo getTableColumn(@Param("table_name") String tableName, @Param("column_name") String columnName);
}
